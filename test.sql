# Написать SQL запрос, который вернет сумму, полученную по причине refund за последние 7 дней.

SELECT sum(amount) as sum, currency
FROM transactions
WHERE reason = 'refund'
  AND transaction_type = 'debit'
  AND created_at >= DATE(NOW() - INTERVAL 7 DAY)
GROUP BY currency;
