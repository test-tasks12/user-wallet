<?php

namespace UserWallet\Http\Controllers;

use Exception;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use UserWallet\Dtos\TransactionDto;
use UserWallet\Models\Wallet;
use UserWallet\Repository\WalletRepository;
use UserWallet\Services\TransactionService;
use function json_decode;
use function json_encode;

class TransactionController
{
    protected WalletRepository $walletRepository;

    public function __construct()
    {
        $this->walletRepository = new WalletRepository();
    }

    /**
     * @throws Exception
     */
    public function transaction(Request $request, Response $response, array $args): Response
    {
        $id = (int)$args['id'];
        $wallet = $this->walletRepository->findById($id);
        if (!$wallet) {
            return $response->withStatus(404, 'wallet not found');
        }

        $wallet = new Wallet((int)$wallet[0]->id, (float)$wallet[0]->balance, $wallet[0]->currency);

        $body = json_decode($request->getBody(), true);

        $errors = $this->validateTransactionRequest($body);

        if (count($errors) > 0) {
            $response = $response->withStatus(422);
            $response->getBody()->write(json_encode($errors));
            return $response;
        }

        $transactionService = new TransactionService(
            new TransactionDto(
                $body['transactionType'],
                $body['amount'],
                $body['currency'],
                $body['reason'],
            ),
            $wallet,
        );

        $transactionService->changeBalance();

        $response->getBody()->write(json_encode($body));
        return $response;
    }

    public function validateTransactionRequest(array $body): array
    {
        $errors = [];
        $requiredFields = ['transactionType', 'amount', 'currency', 'reason'];

        foreach ($requiredFields as $field) {
            if (empty($body[$field])) {
                $errors[$field] = "$field is required";
            }
        }

        return $errors;
    }
}
