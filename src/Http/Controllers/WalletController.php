<?php

declare(strict_types=1);

namespace UserWallet\Http\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use UserWallet\Repository\WalletRepository;

class WalletController
{
    protected WalletRepository $walletRepository;

    public function __construct()
    {
        $this->walletRepository = new WalletRepository();
    }

    public function getById(Request $request, Response $response, array $args): Response
    {
        $id = (int)$args['id'];
        $wallet = $this->walletRepository->findById($id);
        if (!$wallet) {
            $response = $response->withStatus(404);
        }
        $response->getBody()->write(json_encode($wallet[0]));
        return $response;
    }
}
