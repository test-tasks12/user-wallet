<?php

namespace UserWallet\Components;

use UserWallet\Exceptions\ExceptionInvalidArgument;

final class TransactionType
{
    public const DEBIT = 'debit';
    public const CREDIT = 'credit';

    private string $transactionType;

    private array $availableTransactionTypes = [self::CREDIT, self::DEBIT];

    private function __construct(string $transactionType)
    {
        if (!in_array($transactionType, $this->availableTransactionTypes)) {
            throw new ExceptionInvalidArgument('Transaction type can be debit or credit.');
        }

        $this->transactionType = $transactionType;
    }

    public static function create(string $transactionType): TransactionType
    {
        return new TransactionType($transactionType);
    }

    public function getValue(): string
    {
        return $this->transactionType;
    }
}
