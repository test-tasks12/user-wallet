<?php

namespace UserWallet\Components;

use UserWallet\Exceptions\ExceptionInvalidArgument;
use UserWallet\Helpers\Helper;

final class Amount
{
    private float $amount;
    private string $currency;

    private array $availableCurrencies = ['RUB', 'USD'];

    private function __construct(float $amount, string $currency)
    {
        if ($amount < 0) {
            throw new ExceptionInvalidArgument('Amount can`t be less than zero');
        }

        $currency = mb_strtoupper($currency);
        if (!in_array($currency, $this->availableCurrencies)) {
            throw new ExceptionInvalidArgument('Currency can be RUB or USD.');
        }

        $this->amount = round($amount, Helper::AMOUNT_PRECISION);
        $this->currency = $currency;
    }

    public static function create(float $amount, string $currency): Amount
    {
        return new self($amount, $currency);
    }

    public function getValue(): float
    {
        return $this->amount;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }
}
