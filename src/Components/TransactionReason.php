<?php

namespace UserWallet\Components;

use UserWallet\Exceptions\ExceptionInvalidArgument;

final class TransactionReason
{
    private string $reason;

    private array $availableReasons = ['stock', 'refund'];

    private function __construct(string $reason)
    {
        if (!in_array($reason, $this->availableReasons)) {
            throw new ExceptionInvalidArgument('Reason type can be ' . implode(', ', $this->availableReasons));
        }

        $this->reason = $reason;
    }

    public static function create(string $reason): TransactionReason
    {
        return new self($reason);
    }

    public function getValue(): string
    {
        return $this->reason;
    }
}
