<?php

namespace UserWallet\Exceptions;

use RuntimeException;
use Throwable;

class ExceptionInvalidArgument extends RuntimeException
{
    /**
     * ExceptionInvalidParameter constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = 'Invalid command', int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
