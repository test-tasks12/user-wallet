<?php

namespace UserWallet\Services;

use UserWallet\Repository\ExchangeRateRepository;

class ExchangeRateSaver
{
    public function __construct(
        protected CurrencyConverter      $currencyConverter,
        protected ExchangeRateRepository $exchangeRateRepository
    )
    {
    }

    public function saveExchangeRate(string $from, string $to): void
    {
        $value = $this->convert($from, $to);
        $this->exchangeRateRepository->save($from, $to, $value);
    }

    private function convert(string $from, string $to): float
    {
        return $this->currencyConverter->from($from)->to($to)->convert(1);
    }
}
