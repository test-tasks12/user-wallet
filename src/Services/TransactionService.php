<?php

namespace UserWallet\Services;

use Exception;
use UserWallet\Components\Amount;
use UserWallet\Components\TransactionType;
use UserWallet\DB\DBConnection;
use UserWallet\Dtos\TransactionDto;
use UserWallet\Models\Wallet;
use UserWallet\Repository\ExchangeRateRepository;
use UserWallet\Repository\TransactionRepository;
use UserWallet\Repository\WalletRepository;

class TransactionService
{
    protected DBConnection $DBConnection;
    protected ExchangeRateRepository $exchangeRateRepository;
    protected WalletRepository $walletRepository;
    protected TransactionRepository $transactionRepository;

    public function __construct(
        protected TransactionDto $transactionDto,
        protected Wallet         $wallet,
    )
    {
        $this->DBConnection = DBConnection::getConnection();
        $this->exchangeRateRepository = new ExchangeRateRepository();
        $this->walletRepository = new WalletRepository();
        $this->transactionRepository = new TransactionRepository();
    }

    /**
     * @throws Exception
     */
    public function changeBalance(): void
    {
        if ($this->wallet->getCurrency() !== $this->transactionDto->getAmount()->getCurrency()) {
            $exchangedAmount = $this->exchangeAmount(
                $this->transactionDto->getAmount()->getCurrency(),
                $this->wallet->getCurrency(),
                $this->transactionDto->getAmount()->getValue(),
            );
            $this->transactionDto->setAmount($exchangedAmount);
        }

        $this->DBConnection->transaction(function () {
            if ($this->transactionDto->getTransactionType()->getValue() === TransactionType::DEBIT) {
                $this->walletRepository->debit($this->wallet->getId(), $this->transactionDto->getAmount()->getValue());
            } elseif ($this->transactionDto->getTransactionType()->getValue() === TransactionType::CREDIT) {
                $this->walletRepository->credit($this->wallet->getId(), $this->transactionDto->getAmount()->getValue());
            }

            $this->transactionRepository->newTransaction($this->wallet, $this->transactionDto);
        });
    }

    /**
     * @param string $fromCurrency
     * @param string $toCurrency
     * @param float $value
     * @return Amount
     * @throws Exception
     */
    private function exchangeAmount(string $fromCurrency, string $toCurrency, float $value): Amount
    {
        $exchangeRate = $this->exchangeRateRepository->getExchangeRate($fromCurrency, $toCurrency);
        if ($exchangeRate === null) {
            throw new Exception("Exchange rate from $fromCurrency to $toCurrency not set");
        }

        return Amount::create($exchangeRate * $value, $toCurrency);
    }
}
