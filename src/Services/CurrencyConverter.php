<?php

namespace UserWallet\Services;

use DateTime;
use SimpleXMLElement;
use UserWallet\Exceptions\ExceptionInvalidParameter;
use UserWallet\Helpers\Helper;

class CurrencyConverter
{
    protected const DATE_FORMAT = 'd/m/Y';

    protected ?string $fromCurrencyName;
    protected ?float $fromCurrencyRate = null;
    protected ?float $toCurrencyAmount = null;
    protected ?string $toCurrencyName;
    protected ?float $toCurrencyRate = null;
    protected int $precision = Helper::AMOUNT_PRECISION;
    protected ?SimpleXMLElement $rateXml = null;
    protected DateTime $date;
    protected array $exchangeRateList = [];

    public function __construct()
    {
        $this->date = new DateTime();
    }

    public function from(string $currencyName): static
    {
        if ($currencyName === '') {
            throw new ExceptionInvalidParameter('The currency code is incorrect.');
        }

        $this->fromCurrencyName = mb_strtoupper($currencyName);
        return $this;
    }

    public function to(string $currencyName): static
    {
        if ($currencyName === '') {
            throw new ExceptionInvalidParameter('The currency code is incorrect.');
        }

        $this->toCurrencyName = mb_strtoupper($currencyName);
        return $this;
    }

    public function precision(int $precision = Helper::AMOUNT_PRECISION): static
    {
        if ($precision <= 0) {
            throw new ExceptionInvalidParameter('Precision must be greater than zero.');
        }

        $this->precision = $precision;
        return $this;
    }

    public function date(DateTime $date): static
    {
        $this->date = $date;
        return $this;
    }

    public function convert(float $amount): float
    {
        if ($this->fromCurrencyName === $this->toCurrencyName) {
            return $amount;
        }

        $this->fromCurrencyRate = $this->GetRate($this->fromCurrencyName);
        $this->toCurrencyRate = $this->GetRate($this->toCurrencyName);

        $result = $amount / $this->fromCurrencyRate * $this->toCurrencyRate;
        $this->toCurrencyAmount = round($result, $this->precision);
        return $this->toCurrencyAmount;
    }

    private function GetRate(string $currency): float
    {
        if (empty($this->exchangeRateList)) {
            $xml = $this->getXML();
            foreach ($xml->Valute as $valute) {
                $value = str_replace(',', '.', $valute->Value);
                $code = (string)$valute->CharCode;
                $this->exchangeRateList[$code] = floatval($valute->Nominal) / floatval($value);
            }

            $this->exchangeRateList['RUB'] = 1;
        }

        if (!array_key_exists($currency, $this->exchangeRateList)) {
            throw new ExceptionInvalidParameter('Invalid currency ' . $currency);
        }

        return $this->exchangeRateList[$currency];
    }

    private function getXML(): SimpleXMLElement
    {
        if (is_null($this->rateXml)) {
            $date = $this->date->format(self::DATE_FORMAT);
            $this->rateXml = \simplexml_load_file('https://www.cbr.ru/scripts/XML_daily.asp?date_req=' . $date);
        }
        return $this->rateXml;
    }
}
