<?php

namespace UserWallet\Dtos;

use UserWallet\Components\Amount;
use UserWallet\Components\TransactionReason;
use UserWallet\Components\TransactionType;

class TransactionDto
{
    private TransactionType $transactionType;
    private Amount $amount;
    private TransactionReason $transactionReason;

    public function __construct(
        string $transactionType,
        float  $amount,
        string $currency,
        string $transactionReason,
    )
    {
        $this->transactionType = TransactionType::create($transactionType);
        $this->amount = Amount::create($amount, $currency);
        $this->transactionReason = TransactionReason::create($transactionReason);
    }

    public function getTransactionType(): TransactionType
    {
        return $this->transactionType;
    }

    public function setAmount(Amount $amount): void
    {
        $this->amount = $amount;
    }

    public function getAmount(): Amount
    {
        return $this->amount;
    }

    public function getTransactionReason(): TransactionReason
    {
        return $this->transactionReason;
    }
}
