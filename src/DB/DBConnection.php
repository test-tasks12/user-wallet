<?php

namespace UserWallet\DB;

use Closure;
use Exception;
use PDO;
use PDOException;
use PDOStatement;
use UserWallet\Config\Config;

class DBConnection
{
    private static ?DBConnection $instance = null;
    private PDO $dbh;

    /**
     * @throws Exception
     */
    private function __construct()
    {
        $config = Config::getInstance()->getDbConfig();
        $database = $config['database'];
        $host = $config['host'];
        $dsn = "mysql:dbname=$database;host=$host";
        $username = $config['username'];
        $password = $config['password'];
        try {
            $this->dbh = new PDO($dsn, $username, $password);
            $this->dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        } catch (Exception $exception) {
            throw new Exception('12312312', previous: $exception);
        }
    }

    public static function getConnection(): self
    {
        if (!self::$instance) {
            self::$instance = new DBConnection();
        }
        return self::$instance;
    }

    /**
     * @param string $sql
     * @param array $params
     * @return array|bool|PDOStatement
     */
    public function runSql(string $sql, array $params = []): array|bool|PDOStatement
    {
        $sth = $this->dbh->prepare($sql);
        foreach ($params as $param => $value) {
            match (gettype($value)) {
                'integer' => $type = PDO::PARAM_INT,
                default => $type = PDO::PARAM_STR,
            };
            $sth->bindValue($param, $value, $type);
        }
        $sth->execute();
        return $sth->fetchAll();
    }

    /**
     * @param Closure $callback
     * @return mixed
     * @throws Exception
     */
    public function transaction(Closure $callback): mixed
    {
        try {
            $this->dbh->beginTransaction();
            $callbackResult = $callback();
            $this->dbh->commit();
        } catch (PDOException $exception) {
            $this->dbh->rollBack();
            throw new Exception('Error when run transaction', previous: $exception);
        }

        return $callbackResult;
    }
}
