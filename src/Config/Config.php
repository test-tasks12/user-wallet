<?php

namespace UserWallet\Config;

use Dotenv\Dotenv;

class Config
{
    private static ?Config $instance = null;

    private static array $config;

    private function __construct()
    {
        $rootDir = realpath(__DIR__ . '/../../');
        $dotenv = Dotenv::createImmutable($rootDir);
        $env = $dotenv->load();
        $dotenv->required([
            'DB_HOST',
            'DB_PORT',
            'DB_DATABASE',
            'DB_USERNAME',
            'DB_PASSWORD',
        ]);

        self::$config = [
            'rootDir' => $rootDir,
            'db' => [
                'host' => $env['DB_HOST'],
                'port' => $env['DB_PORT'],
                'database' => $env['DB_DATABASE'],
                'username' => $env['DB_USERNAME'],
                'password' => $env['DB_PASSWORD'],
            ],
        ];
    }

    public static function getInstance(): Config
    {
        if (is_null(self::$instance)) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    /**
     * get all configs
     *
     * @return array
     */
    public static function getConfig(): array
    {
        return static::$config;
    }

    public static function getDbConfig(): array
    {
        return static::$config['db'];
    }
}
