<?php

declare(strict_types=1);

namespace UserWallet\Repository;

use Closure;
use Exception;
use UserWallet\DB\DBConnection;

abstract class Repository
{
    protected string $tableName;
    protected DBConnection $connection;

    public function __construct()
    {
        $this->connection = DBConnection::getConnection();
        $this->tableName = $this->tableName();
    }

    public abstract function tableName(): string;

    public function findById(int $id): array
    {
        $sql = "SELECT * FROM $this->tableName WHERE id=:id LIMIT 1";
        return $this->connection->runSql($sql, [':id' => $id]);
    }

    /**
     * @param Closure $callback
     * @return mixed
     * @throws Exception
     */
    public function transaction(Closure $callback): mixed
    {
        return $this->connection->transaction($callback);
    }
}
