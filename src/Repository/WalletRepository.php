<?php

namespace UserWallet\Repository;

class WalletRepository extends Repository
{
    public function tableName(): string
    {
        return 'wallets';
    }

    public function debit(int $id, float $amount)
    {
        $sql = "UPDATE $this->tableName SET `balance` = `balance` - :amount WHERE id =:id";
        $this->connection->runSql($sql, [
            ':amount' => $amount,
            ':id' => $id,
        ]);
    }

    public function credit(int $id, float $amount)
    {
        $sql = "UPDATE $this->tableName SET `balance` = `balance` + :amount WHERE id =:id";
        $this->connection->runSql($sql, [
            ':amount' => $amount,
            ':id' => $id,
        ]);
    }
}
