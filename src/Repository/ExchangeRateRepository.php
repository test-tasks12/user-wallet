<?php

namespace UserWallet\Repository;

class ExchangeRateRepository extends Repository
{

    public function tableName(): string
    {
        return 'exchange_rates';
    }

    public function save(string $from, string $to, float $value)
    {
        $sql = "INSERT INTO $this->tableName (`from`, `to`, `value`, `updated_at`) VALUES (:from, :to, :value, now()) ON DUPLICATE KEY UPDATE `value`=:value, `updated_at`=now()";

        $this->connection->runSql($sql, [
            ':from' => $from,
            ':to' => $to,
            ':value' => $value,
        ]);
    }

    public function getExchangeRate(string $from, string $to): ?float
    {
        $sql = "SELECT value FROM $this->tableName WHERE `from`=:from AND `to`=:to";
        $result = $this->connection->runSql($sql, [
            ':from' => $from,
            ':to' => $to,
        ]);
        if (!empty($result)) {
            return floatval($result[0]->value);
        }
        return null;
    }
}
