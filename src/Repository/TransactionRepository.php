<?php

namespace UserWallet\Repository;

use UserWallet\Dtos\TransactionDto;
use UserWallet\Models\Wallet;

class TransactionRepository extends Repository
{

    public function tableName(): string
    {
        return 'transactions';
    }

    public function newTransaction(Wallet $wallet, TransactionDto $transactionDto)
    {
        $sql = "INSERT INTO $this->tableName (`wallet_id`, `transaction_type`, `amount`, `currency`,`reason`, `created_at`) 
                                      VALUES (:wallet_id, :transaction_type, :amount, :currency, :reason, now())";

        $this->connection->runSql($sql, [
            ':wallet_id' => $wallet->getId(),
            ':transaction_type' => $transactionDto->getTransactionType()->getValue(),
            ':amount' => $transactionDto->getAmount()->getValue(),
            ':currency' => $transactionDto->getAmount()->getCurrency(),
            ':reason' => $transactionDto->getTransactionReason()->getValue(),
        ]);
    }
}
