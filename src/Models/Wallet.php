<?php

namespace UserWallet\Models;

class Wallet
{
    public function __construct(
        private int    $id,
        private float  $balance,
        private string $currency,
    )
    {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getBalance(): float
    {
        return $this->balance;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }
}
