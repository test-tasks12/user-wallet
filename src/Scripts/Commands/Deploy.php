<?php

namespace UserWallet\Scripts\Commands;

use Exception;
use UserWallet\DB\DBConnection;

class Deploy extends Command
{
    private DBConnection $DBConnection;

    public function __construct()
    {
        $this->DBConnection = DBConnection::getConnection();
    }

    /**
     * @throws Exception
     */
    public function handle(): int
    {
        echo "Start Deploy\n";


        echo "Start create tables\n";
        $this->DBConnection->runSql(
            "  /*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
                    /*!40101 SET NAMES utf8 */;
                    /*!50503 SET NAMES utf8mb4 */;
                    /*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
                    /*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
                    /*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;"
        );
        $this->createExchangeRatesTable();
        $this->createTransactionsTable();
        $this->createUsersTable();
        $this->createWalletsTable();
        echo "Finish create tables\n";

        echo "Start fill tables\n";
        $this->fillExchangeRatesTable();
        $this->fillTransactionsTable();
        $this->fillUsersTable();
        $this->fillWalletsTable();
        echo "Finish fill tables\n";

        $this->DBConnection->runSql(
            "  /*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
                    /*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
                    /*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
                    /*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;"
        );

        echo "Deploy finished\n";
        return 0;
    }

    private function createExchangeRatesTable()
    {
        $this->DBConnection->runSql(
            "CREATE TABLE IF NOT EXISTS `exchange_rates` (
                  `from` char(3) NOT NULL,
                  `to` char(3) NOT NULL,
                  `value` decimal(20,2) NOT NULL,
                  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
                  PRIMARY KEY (`from`,`to`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;"
        );
    }

    private function createTransactionsTable()
    {
        $this->DBConnection->runSql(
            "CREATE TABLE IF NOT EXISTS `transactions` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `wallet_id` int(11) NOT NULL,
                  `transaction_type` char(6) NOT NULL,
                  `amount` decimal(20,2) NOT NULL,
                  `currency` char(3) NOT NULL,
                  `reason` varchar(50) NOT NULL,
                  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
                  PRIMARY KEY (`id`),
                  KEY `FK_transactions_wallets` (`wallet_id`),
                  CONSTRAINT `FK_transactions_wallets` FOREIGN KEY (`wallet_id`) REFERENCES `wallets` (`id`)
                ) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;"
        );
    }

    private function createUsersTable()
    {
        $this->DBConnection->runSql(
            "CREATE TABLE IF NOT EXISTS `users` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `name` varchar(50) NOT NULL,
                  `wallet_id` int(11) NOT NULL,
                  PRIMARY KEY (`id`),
                  KEY `FK_users_wallets` (`wallet_id`),
                  CONSTRAINT `FK_users_wallets` FOREIGN KEY (`wallet_id`) REFERENCES `wallets` (`id`)
                ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;"
        );
    }

    private function createWalletsTable()
    {
        $this->DBConnection->runSql(
            "CREATE TABLE IF NOT EXISTS `wallets` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `balance` decimal(20,2) NOT NULL,
                  `currency` char(3) NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;"
        );
    }

    private function fillExchangeRatesTable()
    {
        $this->DBConnection->runSql(
            "
                DELETE FROM `exchange_rates`;
                /*!40000 ALTER TABLE `exchange_rates` DISABLE KEYS */;
                INSERT INTO `exchange_rates` (`from`, `to`, `value`, `updated_at`) VALUES
                    ('RUB', 'USD', 0.02, '2022-06-13 15:00:40'),
                    ('USD', 'RUB', 57.78, '2022-06-13 15:00:27');
                /*!40000 ALTER TABLE `exchange_rates` ENABLE KEYS */;
            "
        );
    }

    private function fillTransactionsTable()
    {
        $this->DBConnection->runSql(
            "DELETE FROM `transactions`;
            /*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
                    INSERT INTO `transactions` (`id`, `wallet_id`, `transaction_type`, `amount`, `currency`, `reason`, `created_at`) VALUES
                        (1, 1, 'credit', 1.00, 'RUB', 'refund', '2022-06-13 21:56:19'),
                        (2, 2, 'credit', 1.00, 'USD', 'refund', '2022-06-13 21:56:52');
                    /*!40000 ALTER TABLE `transactions` ENABLE KEYS */;"
        );
    }

    private function fillUsersTable()
    {
        $this->DBConnection->runSql(
            "DELETE FROM `users`;
                /*!40000 ALTER TABLE `users` DISABLE KEYS */;
                INSERT INTO `users` (`id`, `name`, `wallet_id`) VALUES
                    (1, 'Dima', 1),
                    (2, 'John', 2);
                /*!40000 ALTER TABLE `users` ENABLE KEYS */;"
        );
    }

    private function fillWalletsTable()
    {
        $this->DBConnection->runSql(
            "DELETE FROM `wallets`;
                /*!40000 ALTER TABLE `wallets` DISABLE KEYS */;
                INSERT INTO `wallets` (`id`, `balance`, `currency`) VALUES
                    (1, 1.00, 'RUB'),
                    (2, 1.00, 'USD');
                /*!40000 ALTER TABLE `wallets` ENABLE KEYS */;"
        );
    }
}
