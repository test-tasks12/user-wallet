<?php

namespace UserWallet\Scripts\Commands;

use UserWallet\Repository\ExchangeRateRepository;
use UserWallet\Services\CurrencyConverter;
use UserWallet\Services\ExchangeRateSaver;

class SaveExchangeRatesCommand extends Command
{
    protected ExchangeRateSaver $exchangeRateSaver;

    public function __construct()
    {
        $this->exchangeRateSaver = new ExchangeRateSaver(new CurrencyConverter(), new ExchangeRateRepository());
    }

    public function handle(): int
    {
        $this->exchangeRateSaver->saveExchangeRate('USD', 'RUB');
        $this->exchangeRateSaver->saveExchangeRate('RUB', 'USD');
        return 0;
    }
}
