<?php

namespace UserWallet\Scripts\Commands;

abstract class Command
{
    public abstract function handle(): int;
}
