<?php

declare(strict_types=1);

use UserWallet\Exceptions\ExceptionInvalidCommand;
use UserWallet\Scripts\Commands\Deploy;
use UserWallet\Scripts\Commands\SaveExchangeRatesCommand;

require __DIR__ . '/../../vendor/autoload.php';

$command = $_SERVER['argv'][1];

if (empty($command)) {
    throw new ExceptionInvalidCommand('Please enter command name');
}

$commands = [
    'update-exchange-rate' => SaveExchangeRatesCommand::class,
    'deploy' => Deploy::class,
];

if (!array_key_exists($command, $commands)) {
    throw new ExceptionInvalidCommand('Command not found');
}

$handler = new $commands[$command]();

echo $handler->handle();
