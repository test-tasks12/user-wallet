<?php

namespace ConfigTest;

use PHPUnit\Framework\TestCase;
use UserWallet\Config\Config;


class ConfigTest extends TestCase
{
    private array $configKeys = [
        'rootDir',
        'db',
    ];

    public function testConfigKeys()
    {
        $this->assertEquals(array_keys(Config::getInstance()->getConfig()), $this->configKeys);
    }
}
