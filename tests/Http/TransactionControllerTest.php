<?php

namespace Http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use PHPUnit\Framework\TestCase;

class TransactionControllerTest extends TestCase
{
    /**
     * @throws GuzzleException
     */
    public function testTransaction()
    {
        $client = new Client([
            'base_uri' => 'http://127.0.0.1:8082',
            'headers' => ['Content-Type' => 'application/json']
        ]);

        $response = $client->post('wallets/1/transaction', [
            'body' => json_encode([
                'transactionType' => 'debit',
                'amount' => 1200,
                'currency' => 'rub',
                'reason' => 'refund',
            ])
        ]);

        $this->assertEquals(200, $response->getStatusCode());
    }
}
