<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use UserWallet\Http\Controllers\TransactionController;
use UserWallet\Http\Controllers\WalletController;

require __DIR__ . '/../vendor/autoload.php';

$app = AppFactory::create();

$app->get('/wallets/{id}', function (Request $request, Response $response, array $args) {
    return (new WalletController())->getById($request, $response, $args);
});

$app->post('/wallets/{id}/transaction', function (Request $request, Response $response, array $args) {
    return (new TransactionController())->transaction($request, $response, $args);
});


$app->run();
