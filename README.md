# Test Task

## Install steps

<hr>

1 step

```shell
composer install
```

2 step

```
set envirements to .env file
```

3 step

```shell
php src/Scripts/index.php deploy
```

4 step

```shell
bash ./server.sh
```

or run

```shell
cd public && php -S 127.0.0.1:8082
```

## Pages

### Wallets

```
method - GET
http://127.0.0.1:8082/wallets/{id}
```

example

```
curl http://127.0.0.1:8082/wallets/1
```

### Transactions

```
method - POST
http://127.0.0.1:8082/wallets/{id}/transaction
```

example

```
curl -X POST https://reqbin.com/echo/post/json
   -H 'Content-Type: application/json'
   -d '{"transactionType": "credit","amount": 15,"currency": "usd","reason": "refund"}'
```
